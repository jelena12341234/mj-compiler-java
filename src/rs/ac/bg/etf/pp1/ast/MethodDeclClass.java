// generated with ast extension for cup
// version 0.8
// 9/1/2021 17:29:36


package rs.ac.bg.etf.pp1.ast;

public class MethodDeclClass extends MethodDecl {

    private MethodReturnName MethodReturnName;
    private VarList VarList;
    private StatementList StatementList;

    public MethodDeclClass (MethodReturnName MethodReturnName, VarList VarList, StatementList StatementList) {
        this.MethodReturnName=MethodReturnName;
        if(MethodReturnName!=null) MethodReturnName.setParent(this);
        this.VarList=VarList;
        if(VarList!=null) VarList.setParent(this);
        this.StatementList=StatementList;
        if(StatementList!=null) StatementList.setParent(this);
    }

    public MethodReturnName getMethodReturnName() {
        return MethodReturnName;
    }

    public void setMethodReturnName(MethodReturnName MethodReturnName) {
        this.MethodReturnName=MethodReturnName;
    }

    public VarList getVarList() {
        return VarList;
    }

    public void setVarList(VarList VarList) {
        this.VarList=VarList;
    }

    public StatementList getStatementList() {
        return StatementList;
    }

    public void setStatementList(StatementList StatementList) {
        this.StatementList=StatementList;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(MethodReturnName!=null) MethodReturnName.accept(visitor);
        if(VarList!=null) VarList.accept(visitor);
        if(StatementList!=null) StatementList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(MethodReturnName!=null) MethodReturnName.traverseTopDown(visitor);
        if(VarList!=null) VarList.traverseTopDown(visitor);
        if(StatementList!=null) StatementList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(MethodReturnName!=null) MethodReturnName.traverseBottomUp(visitor);
        if(VarList!=null) VarList.traverseBottomUp(visitor);
        if(StatementList!=null) StatementList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("MethodDeclClass(\n");

        if(MethodReturnName!=null)
            buffer.append(MethodReturnName.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(VarList!=null)
            buffer.append(VarList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(StatementList!=null)
            buffer.append(StatementList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [MethodDeclClass]");
        return buffer.toString();
    }
}
