package rs.ac.bg.etf.pp1;

import java.util.*;
import rs.ac.bg.etf.pp1.ast.*;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.*;


public class CodeGenerator extends VisitorAdaptor {

	private int mainPc;
	Obj currentMethod = null;	
	public static int eq = 0, dif = 1, lt = 2, lteq = 3, gt = 4, gteq = 5;
	int relOpKind = -1;
	
	Stack<List<Integer>> elseAdrCond = new Stack<>(); //za skok kada condition == false
	Stack<List<Integer>> jumpElse = new Stack<>(); //da se preskoci grana sa elsom 
	
	Stack<Integer> doAdr = new Stack<>();
	Stack<Integer> breakAdr = new Stack<>();
	Stack<ArrayList<Integer>> continueAdr = new Stack<>();
	
	List<Integer> orAdr = new ArrayList<>();
	List<Integer> andAdr = new ArrayList<>();
	
	
	
	public int getMainPc() {
		return mainPc;
	}
	
	//===============================USLOVI====================================
	public void visit(EqualsOpClass relCl) { relOpKind = CodeGenerator.eq; }
	public void visit(DifOpClass relCl) { relOpKind = CodeGenerator.dif; }
	public void visit(GtOpClass relCl) { relOpKind = CodeGenerator.gt; }
	public void visit(GtEqOpClass relCl) { relOpKind = CodeGenerator.gteq; }
	public void visit(LtOpClass relCl) {  relOpKind = CodeGenerator.lt; }
	public void visit(LtEqClass relCl) {  relOpKind = CodeGenerator.lteq; }

	
	public void visit(OrClass orCl) {
		Code.putFalseJump(Code.inverse[relOpKind], 0);
		orAdr.add(Code.pc - 2);		
		
		for(int i = 0; i < this.andAdr.size(); i++) {
			Code.put2(this.andAdr.get(i), Code.pc - this.andAdr.get(i) + 1);
		}
		this.andAdr.clear();
	}
	
	public void visit(AndClass andCl) {
		Code.putFalseJump(relOpKind, 0);
		this.andAdr.add(Code.pc - 2);				
	}
	
	//=====================================SWITCH==================================
	
	Stack<ArrayList<Integer>> swAdr = new Stack<>();
	Stack<Integer> lastJumpSwitch = new Stack<>();
	
	public void visit(CaseClass clCl) {
		if(!swAdr.isEmpty()) {
			ArrayList<Integer> temp = swAdr.peek();
			Code.put2(temp.get(0), Code.pc - temp.get(0) + 1);
			temp.remove(0);
		}
	}
	
	public void visit(StatementSWITCHCASEClass swCl) {
		swAdr.pop();
			
		for(int i = 0; i < this.breakAdr.size(); i++) {
			Code.put2(this.breakAdr.get(i),	Code.pc - this.breakAdr.get(i) + 1);
		}
		this.breakAdr.clear();
		
		if(!this.lastJumpSwitch.isEmpty()) {
			int temp = this.lastJumpSwitch.pop();
			Code.put2(temp, Code.pc - temp + 1);
		}
	}
	
	public void visit(ExpressionExpr1Class exprCl) {
		SyntaxNode parent = exprCl.getParent();
		if(parent.getClass() == StatementSWITCHCASEClass.class) {
			SwitchVisitor swVis = new SwitchVisitor();
			parent.traverseTopDown(swVis);

			Stack<Integer> temp = swVis.getStack();
			ArrayList<Integer> swJumps = new ArrayList<>();
			ArrayList<Integer> nextCondJmp = new ArrayList<>();
			
			int cnt = swVis.getStack().size();
			
			for(int i = 0; i < cnt; i++) {
				if(!nextCondJmp.isEmpty()) {
					Code.put2(nextCondJmp.get(0), Code.pc - nextCondJmp.get(0) + 1);
					nextCondJmp.clear();
				}
				if(i != cnt - 1) Code.put(Code.dup);
		
				Code.loadConst(temp.pop());
				if(i != cnt - 1) Code.putFalseJump(eq, 0);
				else Code.putFalseJump(Code.inverse[eq], 0);
				nextCondJmp.add(Code.pc - 2);
				if(i != cnt - 1) {Code.put(Code.pop);
				Code.putJump(0);
				}
				swJumps.add(Code.pc - 2);
			}
	
				swAdr.push(swJumps);
				Code.putJump(0);
				lastJumpSwitch.push(Code.pc - 2);
		}
	}
	
	public void visit(ExpressionClass exprCl) {
		SyntaxNode parent = exprCl.getParent();
		if(parent.getClass() == StatementSWITCHCASEClass.class) {
			SwitchVisitor swVis = new SwitchVisitor();
			parent.traverseTopDown(swVis);
			
			for(int i = 0; i < swVis.getStack().size() - 1; i++) Code.put(Code.dup);
			
			Stack<Integer> temp = swVis.getStack();
			ArrayList<Integer> swJumps = new ArrayList<>();
			
			while(!temp.isEmpty()) {
				Code.loadConst(temp.pop());
				Code.putFalseJump(Code.inverse[eq], 0);
				swJumps.add(Code.pc - 2);
			}
				swAdr.push(swJumps);
		}
		
		exprEnd();
	}
	
	
	//=====================================IF-ELSE===================================
	
	public void visit(ElseClass elseClass) {
		ArrayList<Integer> jmp = new ArrayList<>();
		Code.putJump(0); jmp.add(Code.pc - 2);
		this.jumpElse.push(jmp);
		List<Integer> temp = this.elseAdrCond.pop();
		
		for(int i=0; i < temp.size(); i++) {
			Code.put2(temp.get(i), Code.pc - temp.get(i) + 1);
		}
		
		for(int i = 0; i < this.andAdr.size(); i++) {
			Code.put2(this.andAdr.get(i), Code.pc - this.andAdr.get(i) + 1);
		}
		this.andAdr.clear();
		if(!this.orAdr.isEmpty()) { Code.put2(this.orAdr.get(this.orAdr.size() - 1), Code.pc - this.orAdr.get(this.orAdr.size() - 1) + 1); this.orAdr.clear();}
	}
	
	public void visit(ConditionClass condClass) {
		SyntaxNode parent = condClass.getParent();
		
		if(parent.getClass() != ConditionOrClass.class) {
			if(!this.elseAdrCond.empty()) {
				Code.putFalseJump(relOpKind, 0);
				List<Integer> temp = this.elseAdrCond.pop();
				temp.add(Code.pc - 2); this.elseAdrCond.push(temp);
				ifDetected = false;
			}
		}
	}
	
	public void visit(ConditionOrClass condClass) {
		SyntaxNode parent = condClass.getParent();
		if(parent.getClass() != ConditionOrClass.class) {
			Code.putFalseJump(relOpKind, 0);
			orAdr.add(Code.pc - 2);		
			
			for(int i = 0; i < this.orAdr.size() - 1; i++) 
				Code.put2(this.orAdr.get(i), Code.pc - this.orAdr.get(i) + 1);
		}
		
	}
	
	boolean ifDetected = false; //ovo treba da bude stek(u slucaju ugnezdenih)
	
	public void visit(IfClass ifClass) {
		ifDetected = true;
		this.elseAdrCond.push(new ArrayList<Integer>());
	}
	
	public void visit(StatementIFClass elseCl) {
		List<Integer> temp = this.elseAdrCond.pop();
		for(int i=0; i < temp.size(); i++) {
			Code.put2(temp.get(i), Code.pc - temp.get(i) + 1);
		}
		for(int i = 0; i < this.andAdr.size(); i++) {
			Code.put2(this.andAdr.get(i), Code.pc - this.andAdr.get(i) + 1);
		}
		this.andAdr.clear();
		
		if(!this.orAdr.isEmpty()) { Code.put2(this.orAdr.get(this.orAdr.size() - 1), Code.pc - this.orAdr.get(this.orAdr.size() - 1) + 1); this.orAdr.clear();}
	}
	
	public void visit(StatementIFELSEClass ifElCl) {
		List<Integer> temp = this.jumpElse.pop();
		for(int i=0; i < temp.size(); i++) {
			Code.put2(temp.get(i), Code.pc - temp.get(i) + 1);
		}
		for(int i = 0; i < this.andAdr.size(); i++) {
			Code.put2(this.andAdr.get(i), Code.pc - this.andAdr.get(i) + 1);
		}
		this.andAdr.clear();
	}
	
	//=====================================BREAK&SWITCH===================================
	public void visit(StatementBREAKClass breakCl) {
		Code.putJump(0);
		this.breakAdr.push(Code.pc - 2);
	}
	
	public void visit(StatementCONTINUEClass breakCl) {
		Code.putJump(0);
		ArrayList<Integer> temp = this.continueAdr.pop();
		temp.add(Code.pc - 2); this.continueAdr.push(temp);
	}
	
	//=====================================DO-WHILE===================================
	
	public void visit(DoClass doAdr) {
		this.doAdr.push(Code.pc);
		this.continueAdr.push(new ArrayList<>());
	}	
	
	public void visit(WhileClass whileCl) {
		ArrayList<Integer> temp = this.continueAdr.pop();
		for(int i = 0; i < temp.size(); i++) {
			Code.put2(temp.get(i), Code.pc - temp.get(i) + 1);
		}
	}
	
	public void visit(StatementDOWHILEClass doWhCl) {
		Code.putFalseJump(Code.inverse[relOpKind], this.doAdr.pop());
		if(!this.breakAdr.empty()) {
			int temp = this.breakAdr.pop();
			Code.put2(temp, Code.pc - temp + 1);
		}
	}	
	
	//=====================================OSNOVNE METODE===================================
	public void visit(ProgName progName) {
		Tab.lenObj.setAdr(Code.pc);
		Code.put(Code.enter); // enter 0 0
		Code.put(0);
		Code.put(0);
		Code.put(Code.arraylength);
		Code.put(Code.exit);
		Code.put(Code.return_);

		Tab.chrObj.setAdr(Code.pc);
		Code.put(Code.enter); // enter 0 0
		Code.put(0);
		Code.put(0);
		Code.put(Code.exit);
		Code.put(Code.return_);

		Tab.ordObj.setAdr(Code.pc);
		Code.put(Code.enter); // enter 0 0
		Code.put(0);
		Code.put(0);
		Code.put(Code.exit);
		Code.put(Code.return_);
	}
	//=====================================ZA PRINT&READ=====================================
	
	public void visit(StatementPRINTClass print) {
		if(print.getExpression().struct == Tab.intType || print.getExpression().struct == SemanticAnalyzer.boolType) {
			Code.loadConst(5);
			Code.put(Code.print);
		} else if(print.getExpression().struct == Tab.charType) {
			Code.loadConst(1);
			Code.put(Code.bprint);
		} 
	}

	public void visit(StatementPRINTNUMClass print) {
		if(print.getExpression().struct == Tab.intType || print.getExpression().struct == SemanticAnalyzer.boolType) {
			Code.loadConst(print.getN());
			Code.put(Code.print);
		} else if(print.getExpression().struct == Tab.charType) {
			Code.loadConst(print.getN());
			Code.put(Code.bprint);
		} 
	}
	
	public void visit(StatementREADClass readCl) {
		if (readCl.getDesignator().obj.getType() == Tab.charType) {
			Code.put(Code.bread);
		} else {
			Code.put(Code.read);
		}
		Code.store(readCl.getDesignator().obj);
	}
	
	//=====================================ZA KONSTANTE=====================================
	
	public void visit(ConstValueNUMClass cnst) {
		Obj o = Tab.insert(Obj.Con, "$", cnst.struct );
		o.setLevel(0);
		o.setAdr(cnst.getN());
		Code.load(o);
	}

	public void visit(ConstValueBOOLClass cnst) {
		Obj o = Tab.insert(Obj.Con, "$", cnst.struct );
		o.setLevel(0);
		if(cnst.getB().equals("false"))
		o.setAdr(0); else o.setAdr(1);
		Code.load(o);
		
		if(ifDetected) {
			Code.put(Code.const_1);this.relOpKind = eq;
			ifDetected = false;
		}
	}
	
	public void visit(ConstValueCHARClass cnst) {
		Obj o = Tab.insert(Obj.Con, "$", cnst.struct );
		o.setLevel(0);
		o.setAdr(cnst.getC());
		Code.load(o);
	}
	//===================================ZA KLASE===========================================
	List<Obj> classes = SemanticAnalyzer.classes;
	HashMap<String, String> izvOsn = SemanticAnalyzer.izvOsn;
	int last_value = Code.dataSize;
	
	
	//popunjavanje tabele virtuelnih fja
	public void createTVF() {
		for(int i = 0; i < classes.size(); i++) {
			Collection<Obj> collection = classes.get(i).getType().getMembers();
			List<Obj> temp = new ArrayList<Obj>(collection);
			Obj tvf_field = (Obj) temp.get(0); tvf_field.setAdr(last_value);
			temp.set(0, tvf_field);
			
			if(collection != null) {
				
				for(Obj o : collection) {
					if(o.getKind() == Obj.Meth) {
						String funcName = o.getName();
						for(int j = 0; j < funcName.length(); j++) {
							Code.loadConst(funcName.charAt(j));
							Code.put(Code.putstatic);
							Code.put2(last_value++); Code.dataSize++;
						}
						//za kraj naziva funkcije
						Code.loadConst(-1);
						Code.put(Code.putstatic);
						Code.put2(last_value++);  Code.dataSize++;
						
						//za adresu funkcije						
						Collection<Obj> coll2 = o.getLocalSymbols();
						for(Obj o2 : coll2) {
							if(o2.getName().equals("this")) {
								if(o2.getType().getElemType() != null) Code.loadConst(o.getAdr());
								else {
										int tmp = getFuncAdr(classes.get(i).getName(), o.getName());
										Code.loadConst(tmp!=-1? tmp : o.getAdr());
									}
							}
						}
					
						//za adresu funkcije
						Code.put(Code.putstatic);
						Code.put2(last_value++);  Code.dataSize++;
					}
				}
			}
			//za kraj klase
			Code.loadConst(-2);
			Code.put(Code.putstatic);
			Code.put2(last_value++);  Code.dataSize++;
		}	
	}
	
	public int getFuncAdr(String className, String funcName) {
		String osn = izvOsn.get(className);
		
		while(osn!=null) {
			for(int i = 0; i < this.classes.size(); i++) {
				if(this.classes.get(i).getName().equals(osn)) {
					Collection<Obj> tempColl = this.classes.get(i).getType().getMembers();
					
					for(Obj o : tempColl) {
						if(o.getName().equals(funcName)) return o.getAdr();
					}
				}
			}
			osn = izvOsn.get(osn);
		}
		return -1;
	}
	
	//kreiranje objekata
	boolean newClassDetected = false;
	public void visit(FactorNTypeClass newCl) {
		newClassDetected = true;
		for(int i = 0; i < this.classes.size(); i++) {
			if(this.classes.get(i).getName().equals(this.typeName)) {
				Code.put(Code.new_);
				Code.put2(this.classes.get(i).getLevel() * 4);
			}
		}	
	}
	
	
	String typeName = "";
	public void visit(TypeClass typeCl) {
		SyntaxNode parent = typeCl.getParent();
		
		if(parent.getClass() == FactorNTypeClass.class) {
			typeName = typeCl.getTypeName();
		}
	}
	
	
	Obj classDetected = null;
	
	 //za generisanje isparvnog koda 
	public void visit(ClassDefClass classCl) {
	 	for(int i = 0; i < this.classes.size(); i++)
	 		if(classCl.getName().equals(classes.get(i).getName())) 
	 			classDetected = classes.get(i);
	}
	
	public void visit(ClassDefExtClass classCl) {
	 	for(int i = 0; i < this.classes.size(); i++)
	 		if(classCl.getName().equals(classes.get(i).getName())) 
	 			classDetected = classes.get(i);
	}	
	
	public void visit(ClassDeclClass clCl) {
		classDetected = null;		
	}
	
	public void visit(ClassDeclMTDListClass clCl) {
		classDetected = null;
	}
	
	
	//===================================ZA METODE==========================================
	
	public void visit(MethodReturnNameClass methodTypeName) {
		methodTypeName.obj.setAdr(Code.pc);
		currentMethod = methodTypeName.obj;
	
		// Generate the entry
		Code.put(Code.enter);
		Code.put(currentMethod.getLevel());
		Code.put(currentMethod.getLocalSymbols().size());	
	}
	
	public void visit(MethodVoidClass methodTypeName) {
		if("main".equalsIgnoreCase(methodTypeName.getMName())){
		
			mainPc = Code.pc;
			createTVF();
		}
		methodTypeName.obj.setAdr(Code.pc);
		currentMethod = methodTypeName.obj;
	
		// Generate the entry
		Code.put(Code.enter);
		Code.put(currentMethod.getLevel());
		Code.put(currentMethod.getLocalSymbols().size());	
	}
	
	// bez argumenata
	public void visit(MethodDeclClass MethodDeclClass) {
		Code.put(Code.exit);
		Code.put(Code.return_);
		currentMethod = null;
	}

		// sa argumentima
	public void visit(MethodDeclFParsClass MethodDeclFParsClass) {
		Code.put(Code.exit);
		Code.put(Code.return_);
		currentMethod = null;
	}
	
	//=================DESIGNATORI==========================
	
	boolean visited = false;
	Obj desObj = null;
	
	public void visit(DesignatorListClass desCl) {
		desObj = desCl.getDesignator().obj;
	}
	
	public void visit(DesignatorStatementAssignopClass desCl) {
		if(desCl.getDesignator().obj.getKind() == Obj.Elem) {
			Code.put(Code.astore);
		} else Code.store(desCl.getDesignator().obj);
		
		
		
		//pri alociranju objekata klase
		if(newClassDetected) {
			for(int i = 0; i < this.classes.size(); i++) {
				if(this.classes.get(i).getName().equals(this.typeName)) {
					if(desCl.getDesignator().obj.getType() != null && desCl.getDesignator().obj.getKind() == Obj.Fld && desCl.getDesignator().obj.getType().getKind() == Struct.Class) {
						Code.load(desObj);
					} 
					Code.load(desCl.getDesignator().obj);
					
					//ucitavanje TVF-a
					Collection<Obj> tempColl = this.classes.get(i).getType().getMembers();
					for(Obj o : tempColl) 
						if(o.getName().equals("TVF")) 
							Code.loadConst(o.getAdr());
					
					
					Code.put(Code.putfield);
					Code.put2(0);
					newClassDetected = false;
				}
			}
		}
	}
	
	
	public void visit(DesignatorClass designator) {
		SyntaxNode parent = designator.getParent();
		
		if(FuncCall.class == parent.getClass() || FuncParamCall.class == parent.getClass()
				|| DesignatorStatementPARENTClass.class == parent.getClass() || DesignatorStatementActParsClass.class == parent.getClass()) 
			if(classDetected != null) {
				Code.put(Code.load_n);
			}
		
		
		
		if(DesignatorStatementAssignopClass.class != parent.getClass() && FuncCall.class != parent.getClass() && FuncParamCall.class != parent.getClass()
				&& DesignatorStatementPARENTClass.class != parent.getClass() && DesignatorStatementActParsClass.class != parent.getClass()
				&& StatementREADClass.class != parent.getClass()){ //&& ProcCall.class != parent.getClass()){
			if(designator.obj.getKind() == Obj.Fld) Code.put(Code.load_n);
			Code.load(designator.obj);
		} else if(DesignatorStatementAssignopClass.class == parent.getClass() && designator.obj.getKind() == Obj.Fld) {Code.put(Code.load_n);}
		
		if(designator.obj.getType() == SemanticAnalyzer.boolType && ifDetected) {
			Code.put(Code.const_1); this.relOpKind = eq;
			ifDetected = false;
		}
	}
	
	public void visit(FuncParamCall funcCall) {
		Obj functionObj = funcCall.getDesignator().obj;
		int offset = functionObj.getAdr() - Code.pc;
		if(classDetected != null) { //pri deklaraciji klase
			Code.put(Code.load_n);
			Code.put(Code.getfield);
			Code.put2(0);
			
			Code.put(Code.invokevirtual);
			String methName =funcCall.getDesignator().obj.getName();
			for(int i = 0; i < methName.length(); i++) {
				Code.put4(methName.charAt(i));
			}
			Code.put4(-1);
		} else if(classDetected == null && funcCall.getDesignator().obj.getLocalSymbols().toArray().length > 0
				&& !((Obj) funcCall.getDesignator().obj.getLocalSymbols().toArray()[0]).getName().contentEquals("this")) {
			Code.put(Code.call);
			Code.put2(offset);
		} else if(desObj.getKind() == Obj.Elem && desObj.getType().getKind() == Obj.Fld) { //ako je niz klasa u pitanju
			Code.put(Code.dup_x1);
			Code.put(Code.pop);
			Code.put(Code.getfield);
			Code.put2(0);
			
			Code.put(Code.invokevirtual);
			String methName =funcCall.getDesignator().obj.getName();
			for(int i = 0; i < methName.length(); i++) {
				Code.put4(methName.charAt(i));
			}
			Code.put4(-1);
		} else if(desObj.getType().getElemType() != null && desObj.getType().getElemType().getKind() == Struct.Class) { //ako je sam objekat klase u pitanju
			Code.load(desObj);
			Code.put(Code.getfield);
			Code.put2(0);
		
			
			Code.put(Code.invokevirtual);
			String methName =funcCall.getDesignator().obj.getName();
			for(int i = 0; i < methName.length(); i++) {
				Code.put4(methName.charAt(i));
			}
			Code.put4(-1);
		} else if(desObj.getType().getKind() == Struct.Class) {
			Code.load(desObj); 
	//		Code.put(Code.dup);
			Code.put(Code.getfield);
			Code.put2(0);
			
			Code.put(Code.invokevirtual);
			String methName =funcCall.getDesignator().obj.getName();
			for(int i = 0; i < methName.length(); i++) {
				Code.put4(methName.charAt(i));
			}
			Code.put4(-1);
		} else {
			Code.put(Code.call);
			Code.put2(offset);
		}
	
	}
	
	public void visit(FuncCall funcCall) {
		Obj functionObj = funcCall.getDesignator().obj;
		int offset = functionObj.getAdr() - Code.pc;
		if(classDetected != null) { //pri deklaraciji klase
			Code.put(Code.load_n);
			Code.put(Code.getfield);
			Code.put2(0);
			
			Code.put(Code.invokevirtual);
			String methName =funcCall.getDesignator().obj.getName();
			for(int i = 0; i < methName.length(); i++) {
				Code.put4(methName.charAt(i));
			}
			Code.put4(-1);
		} else if(classDetected == null && funcCall.getDesignator().obj.getLocalSymbols().toArray().length > 0
				&& !((Obj) funcCall.getDesignator().obj.getLocalSymbols().toArray()[0]).getName().contentEquals("this")) {
			Code.put(Code.call);
			Code.put2(offset);
		} else if(desObj.getKind() == Obj.Elem && desObj.getType().getKind() == Obj.Fld) { //ako je niz klasa u pitanju
			Code.put(Code.dup_x1);
			Code.put(Code.pop);
			Code.put(Code.getfield);
			Code.put2(0);
			
			Code.put(Code.invokevirtual);
			String methName =funcCall.getDesignator().obj.getName();
			for(int i = 0; i < methName.length(); i++) {
				Code.put4(methName.charAt(i));
			}
			Code.put4(-1);
		} else if(desObj.getType().getElemType() != null && desObj.getType().getElemType().getKind() == Struct.Class) { //ako je sam objekat klase u pitanju
			Code.load(desObj);
			Code.put(Code.getfield);
			Code.put2(0);
		
			
			Code.put(Code.invokevirtual);
			String methName =funcCall.getDesignator().obj.getName();
			for(int i = 0; i < methName.length(); i++) {
				Code.put4(methName.charAt(i));
			}
			Code.put4(-1);
		} else if(desObj.getType().getKind() == Struct.Class) {
			Code.load(desObj); 
		//	Code.put(Code.dup);
			Code.put(Code.getfield);
			Code.put2(0);
			
			Code.put(Code.invokevirtual);
			String methName =funcCall.getDesignator().obj.getName();
			for(int i = 0; i < methName.length(); i++) {
				Code.put4(methName.charAt(i));
			}
			Code.put4(-1);
		} else {
			Code.put(Code.call);
			Code.put2(offset);
		}
	
	}

	public void visit(DesignatorStatementPARENTClass funcCall) {
		Obj functionObj = funcCall.getDesignator().obj;
		int offset = functionObj.getAdr() - Code.pc;
	
		if(classDetected != null) { //pri deklaraciji klase
			Code.put(Code.load_n);
			Code.put(Code.getfield);
			Code.put2(0);
			
			Code.put(Code.invokevirtual);
			String methName =funcCall.getDesignator().obj.getName();
			for(int i = 0; i < methName.length(); i++) {
				Code.put4(methName.charAt(i));
			}
			Code.put4(-1);
		} else if(classDetected == null && funcCall.getDesignator().obj.getLocalSymbols().toArray().length > 0
				&& !((Obj) funcCall.getDesignator().obj.getLocalSymbols().toArray()[0]).getName().contentEquals("this")) {
			Code.put(Code.call);
			Code.put2(offset);
		} else if(desObj.getKind() == Obj.Elem && desObj.getType().getKind() == Obj.Fld) { //ako je niz klasa u pitanju
			Code.put(Code.dup_x1);
			Code.put(Code.pop);
			Code.put(Code.getfield);
			Code.put2(0);
			
			Code.put(Code.invokevirtual);
			String methName =funcCall.getDesignator().obj.getName();
			for(int i = 0; i < methName.length(); i++) {
				Code.put4(methName.charAt(i));
			}
			Code.put4(-1);
		} else if(desObj.getType().getElemType() != null && desObj.getType().getElemType().getKind() == Struct.Class) { //ako je sam objekat klase u pitanju
			Code.load(desObj);
			Code.put(Code.getfield);
			Code.put2(0);
		
			
			Code.put(Code.invokevirtual);
			String methName =funcCall.getDesignator().obj.getName();
			for(int i = 0; i < methName.length(); i++) {
				Code.put4(methName.charAt(i));
			}
			Code.put4(-1);
		} else if(desObj.getType().getKind() == Struct.Class) {
			Code.load(desObj); 
		//	Code.put(Code.dup);
			Code.put(Code.getfield);
			Code.put2(0);
			
			Code.put(Code.invokevirtual);
			String methName =funcCall.getDesignator().obj.getName();
			for(int i = 0; i < methName.length(); i++) {
				Code.put4(methName.charAt(i));
			}
			Code.put4(-1);
		} else {
			Code.put(Code.call);
			Code.put2(offset);
		}
	
		
		if(funcCall.getDesignator().obj.getType() != Tab.noType){
			Code.put(Code.pop);
		}
	}
	
	public void visit(DesignatorStatementActParsClass funcCall) {
		Obj functionObj = funcCall.getDesignator().obj;
		int offset = functionObj.getAdr() - Code.pc;
		
		if(classDetected != null) { //pri deklaraciji klase
			Code.put(Code.load_n);
			Code.put(Code.getfield);
			Code.put2(0);
			
			Code.put(Code.invokevirtual);
			String methName =funcCall.getDesignator().obj.getName();
			for(int i = 0; i < methName.length(); i++) {
				Code.put4(methName.charAt(i));
			}
			Code.put4(-1);
		} else if(classDetected == null && funcCall.getDesignator().obj.getLocalSymbols().toArray().length > 0
				&& !((Obj) funcCall.getDesignator().obj.getLocalSymbols().toArray()[0]).getName().contentEquals("this")) {
			Code.put(Code.call);
			Code.put2(offset);
		} else if(desObj.getKind() == Obj.Elem && desObj.getType().getKind() == Obj.Fld) { //ako je niz klasa u pitanju
			Code.put(Code.dup_x1);
			Code.put(Code.pop); 
			Code.put(Code.getfield);
			Code.put2(0);
			
			Code.put(Code.invokevirtual);
			String methName =funcCall.getDesignator().obj.getName();
			for(int i = 0; i < methName.length(); i++) {
				Code.put4(methName.charAt(i));
			}
			Code.put4(-1);
		} else if(desObj.getType().getElemType() != null && desObj.getType().getElemType().getKind() == Struct.Class) { //ako je sam objekat klase u pitanju
			Code.load(desObj);
			Code.put(Code.getfield);
			Code.put2(0);
		
			
			Code.put(Code.invokevirtual);
			String methName =funcCall.getDesignator().obj.getName();
			for(int i = 0; i < methName.length(); i++) {
				Code.put4(methName.charAt(i));
			}
			Code.put4(-1);
		} else if(desObj.getType().getKind() == Struct.Class) {
			Code.load(desObj); 
	//		Code.put(Code.dup);
			Code.put(Code.getfield);
			Code.put2(0);
			
			Code.put(Code.invokevirtual);
			String methName =funcCall.getDesignator().obj.getName();
			for(int i = 0; i < methName.length(); i++) {
				Code.put4(methName.charAt(i));
			}
			Code.put4(-1);
		} else {
			Code.put(Code.call);
			Code.put2(offset);
		}
		
		
		if(funcCall.getDesignator().obj.getType() != Tab.noType){
			Code.put(Code.pop);
		}
	}
	
	public void visit(DesignatorListExprClass desCl) {
		SyntaxNode parent = desCl.getParent();	
		if(parent.getClass() == FactorDesignatorClass.class || parent.getClass() == DesignatorListClass.class) {
			Code.put(Code.aload); 
		}
		if(parent.getClass() == DesignatorListClass.class) Code.put(Code.dup);
	}
	
	public void visit(ArrName arrName) {
		this.desObj = arrName.getDesignator().obj;
	}

	public void visit(DesignatorStatementINCClass desCl) {
		if(desCl.getDesignator().getClass() == DesignatorListExprClass.class) {
			Code.put(Code.dup2);
			Code.put(Code.aload);
		}
		Code.put(Code.const_1);
		Code.put(Code.add);
		Code.store(desCl.getDesignator().obj);
	}
	
	public void visit(DesignatorStatementDECClass desCl) {
		Code.put(Code.const_1);
		Code.put(Code.sub);
		Code.store(desCl.getDesignator().obj);
	}
	
	//=============================za dodele======================
	public void visit(TermList1Class termCl) {
		if(termCl.getAddOp() instanceof AddOpClass) {
			Code.put(Code.add);
		} else if (termCl.getAddOp() instanceof SubOpClass) {
			Code.put(Code.sub);
		} 
	}
	
	public void visit(TermListClass termCl) {
		if(termCl.getMulOp() instanceof MulOpClass) {
			Code.put(Code.mul);
		} else if (termCl.getMulOp() instanceof DivOpClass) {
			Code.put(Code.div);
		} else if (termCl.getMulOp() instanceof ModOpClass) {
			Code.put(Code.rem);
		} 
	}
	//===============ternarni===========================
	Stack<Integer> ternary = new Stack<>();
	Stack<Integer> end_ = new Stack<>(), else_ = new Stack<>();
	
	public void visit(QMark qmark) {
		Code.put(Code.const_1);
		Code.putFalseJump(Code.eq, 0);
		this.else_.push(Code.pc - 2);
		this.ternary.push(0);		
	}
	
	public void visit(ColonClass clnCl) {
		Code.putJump(0);
		this.end_.push(Code.pc - 2); 
		int temp2 = this.else_.pop();
		Code.put2(temp2, Code.pc - temp2 + 1);
	}
	
	
	
	public void exprEnd() {
		if(!this.end_.isEmpty()) {
			int temp2 = this.end_.pop();
			Code.put2(temp2, Code.pc - temp2 + 1);
		}
	}
	
	
/*	public void visit(QMark qmark) { //dolazis do ternarnog operatora
		Code.put(Code.const_1);
		Code.putFalseJump(Code.eq, 0);
		this.else_.push(Code.pc - 2);
		this.ternary.push(0);
	}

	public void visit(Expression1Class exprCl) {
		if(!this.ternary.isEmpty()) {
			int temp = this.ternary.pop();
			if((temp + 1) % 3 == 1) {
				Code.putJump(0);
				this.end_.push(Code.pc - 2); int temp2 = this.else_.pop();
				Code.put2(temp2, Code.pc - temp2 + 1);
				this.ternary.push(++temp);
			} else if ((temp + 1) % 3 == 2){ 
				int temp2 = this.end_.pop();
				Code.put2(temp2, Code.pc - temp2 + 1);
			}
		}
	}
	*/
	//======================NEW==================================
	
	public void visit(FactorNTypeExprClass newArrCl) {
		Code.put(Code.newarray);
		if(newArrCl.getType().struct == Tab.charType) {
			Code.put(0);
		} else {
			Code.put(1);
		}
	}
	
	//========================================================
	public void visit(NegTermClass negCl) {
		Code.put(Code.neg);
	}
	
	
}
