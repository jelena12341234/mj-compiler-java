package rs.ac.bg.etf.pp1;

import org.apache.log4j.Logger;
import java.util.*;
import rs.ac.bg.etf.pp1.ast.*;
import rs.etf.pp1.symboltable.*;
import rs.etf.pp1.symboltable.concepts.*;
import rs.etf.pp1.symboltable.structure.*;

public class SemanticAnalyzer extends VisitorAdaptor {

	class ListVariables {
		String name;
		boolean primData; // primitive data <= koristim u suprotnom smislu XD
		boolean isArray;
		Struct s;
		Obj o;
		
		public ListVariables(boolean isArray, String name) {
			super();
			this.name = name;
			this.isArray = isArray;
		}

		public ListVariables(String name, Struct s) {
			super();
			this.name = name;
			this.s = s;
		}
		
		public ListVariables(String name, Struct s, Obj o) {
			super();
			this.name = name;
			this.s = s;
			this.o = o;
		}

		public ListVariables(String name, boolean primData) {
			super();
			this.name = name;
			this.primData = primData;
		}

		public ListVariables(String name) {
			super();
			this.name = name;
			this.primData = false;
		}

		public void setPrimData() {
			this.primData = true;
		}
	}

	class ConstData {
		String name;
		int intVal;
		char chVal;
		boolean boolVal;

		public ConstData(String name, int intVal) {
			super();
			this.name = name;
			this.intVal = intVal;
		}

		public ConstData(String name, char chVal) {
			super();
			this.name = name;
			this.chVal = chVal;
		}

		public ConstData(String name, boolean boolVal) {
			super();
			this.name = name;
			this.boolVal = boolVal;
		}
	}

	// =========================promenljive===========================================

	int printCallCount = 0;
	int varDeclCount = 0;
	int formPars = 0; // for formal arguments of a method
	Obj currentMethod = null;
	Obj currentClass = null;
	int nVars;
	boolean errorDetected = false;
	boolean returnFound = false;
	boolean mainMethod = false;
	boolean isArray = false;
	Collection<Obj> collection = null;
	public static HashMap<String,String> izvOsn = new HashMap<>(); 	//key => posmatrana(izvedena) klasa; value => osnovna klasa
														//ako je contains(key) == 0 onda je to osnovna klasa
	Logger log = Logger.getLogger(getClass());

	// =========================Liste==============================================

	ArrayList<ListVariables> vars = new ArrayList<>();
	ArrayList<ListVariables> formParsList = new ArrayList<>(); // formal parameters list
	ArrayList<ListVariables> declaredVars = new ArrayList<>(); // list of all declared variables
	ArrayList<ConstData> constData = new ArrayList<>(); // for constListClass

	static Struct boolType = Tab.insert(Obj.Type, "bool", new Struct(Struct.Bool)).getType();

	// =========================za log==============================================
	public void report_error(String message, SyntaxNode info) {
		errorDetected = true;
		StringBuilder msg = new StringBuilder(message);
		int line = (info == null) ? 0 : info.getLine();
		if (line != 0)
			msg.append(" na liniji ").append(line);
		log.error(msg.toString());
	}

	public void report_info(String message, SyntaxNode info) {
		StringBuilder msg = new StringBuilder(message);
		int line = (info == null) ? 0 : info.getLine();
		if (line != 0)
			msg.append(" na liniji ").append(line);
		log.info(msg.toString());
	}

	// ===================================za  PRINT===================================

	public void visit(StatementPRINTClass print) {
		printCallCount++; 
		Struct temp = retStruct(print.getExpression().struct);
		if (temp != Tab.charType && temp != Tab.intType && temp != boolType) {
			temp = getRetType(calledFunc, Tab.find(currObj).getType());
			if (temp != Tab.charType && temp != Tab.intType && temp != boolType)
			report_error("Greska na liniji " + print.getLine() + " Operand treba biti tipa int, char ili bool", null);
		}
	}

	public void visit(StatementPRINTNUMClass print) {
		printCallCount++;
		Struct temp = retStruct(print.getExpression().struct);
		if (temp != Tab.charType && temp != Tab.intType && temp != boolType) {
			temp = getRetType(calledFunc, Tab.find(currObj).getType());
			if (temp != Tab.charType && temp != Tab.intType && temp != boolType)
			report_error("Greska na liniji " + print.getLine() + " Operand treba biti tipa int, char ili bool", null);
		}
	}

	// ====================================sve za klasu===========================

	int numOfFields = 0;
	
	public void visit(ClassDefClass clCl) {
		if (Tab.find(clCl.getName()) != Tab.noObj) {
			report_error("Greska na liniji " + clCl.getLine() + ": Klasa je vec deklarisana", null);
		}
		report_info("Deklarisana Klasa " + clCl.getName(), clCl);
		Obj obj = Tab.insert(Obj.Type, clCl.getName(), new Struct(Struct.Class));
		currentClass = obj;
		Tab.openScope();
		Tab.insert(Obj.Fld, "TVF", new Struct(Struct.Int)); //struct int jer cuva samo vrednost pokazivaca 
		this.numOfFields = 1;
	}

	public void visit(ClassDefExtClass clCl) {
		if (Tab.find(clCl.getName()) != Tab.noObj) {
			report_error("Greska na liniji " + clCl.getLine() + ": Klasa je vec deklarisana", null);
		}
		report_info("Deklarisana Klasa " + clCl.getName(), clCl);
		izvOsn.put(clCl.getName(), typeName);
		Obj obj = Tab.insert(Obj.Type, clCl.getName(), new Struct(Struct.Class));
		obj.getType().setElementType(clCl.getType().struct);
		currentClass = obj;
		Tab.openScope();
		Tab.insert(Obj.Fld, "TVF", new Struct(Struct.Int)); //struct int jer cuva samo vrednost pokazivaca
		this.numOfFields = 1;
		collection = clCl.getType().struct.getMembers();
		if (collection != null) {
			for (Obj o : collection) {
				if (o.getKind() == Obj.Fld && o.getName() != "TVF") {
					Tab.insert(Obj.Fld, o.getName(), o.getType());
					this.numOfFields++;
				}
			}
		}
	}
	
	public static List<Obj> classes = new ArrayList<>();

	public void visit(ClassDeclClass clCl) {

		if (collection != null) {
			for (Obj o : collection) {
				if (o.getKind() == Obj.Meth && Tab.find(o.getName()) == Tab.noObj) {
					Obj obj = Tab.insert(Obj.Meth, o.getName(), o.getType());
					Tab.openScope();
					Collection<Obj> temp = o.getLocalSymbols();
					for (Obj object : temp) {
						Tab.insert(object.getKind(), object.getName(), object.getType());
					}
					Tab.chainLocalSymbols(obj);
					Tab.closeScope();
				}
			}
			collection = null;
		}

		Tab.chainLocalSymbols(currentClass.getType());
		Tab.closeScope();
		currentClass.setLevel(this.numOfFields + 1); //1 za TVF
		classes.add(currentClass);
		currentClass = null;
		this.numOfFields = 0;
	}

	public void visit(ClassDeclMTDListClass clCl) {

		
		if (collection != null) {
			for (Obj o : collection) {
				if (o.getKind() == Obj.Meth && Tab.find(o.getName()) == Tab.noObj) {
					Obj obj = Tab.insert(Obj.Meth, o.getName(), o.getType());
					Tab.openScope();
					Collection<Obj> temp = o.getLocalSymbols();
					for (Obj object : temp) {
						Tab.insert(object.getKind(), object.getName(), object.getType());
					}
					Tab.chainLocalSymbols(obj);
					Tab.closeScope();
				}
			}
			collection = null;
		}

		currentClass.setLevel(this.numOfFields);
		classes.add(currentClass);
		Tab.chainLocalSymbols(currentClass.getType());
		Tab.closeScope();
		currentClass = null;
		this.numOfFields = 0;
	}

	// ===============================za  var&con==========================================

	public void addVarsList(Struct s) {
		for (int i = 0; i < vars.size(); i++) {
			if (vars.get(i).isArray) {
				Struct formParamArrayType = new Struct(Struct.Array, s);
				Tab.insert(Obj.Var, vars.get(i).name, formParamArrayType);
			} else
				Tab.insert(Obj.Var, vars.get(i).name, s);
		}
		vars.clear();
	}

	public void addFldList(Struct s) {
		for (int i = 0; i < vars.size(); i++) {
			if (vars.get(i).isArray) {
				Struct formParamArrayType = new Struct(Struct.Array, s);
				Tab.insert(Obj.Fld, vars.get(i).name, formParamArrayType);
			} else {
				Tab.insert(Obj.Fld, vars.get(i).name, s);
			}
		}
		vars.clear();
	}

	public void visit(VarDeclClass varCl) {
		varDeclCount++;
		if (Tab.find(varCl.getVarName()) != Tab.noObj) { 
			report_error("Greska na liniji " + varCl.getLine() + ": postojeca vr vec postoji", null);
		}
		if (currentClass != null && currentMethod==null) {
			Tab.insert(Obj.Fld, varCl.getVarName(), varCl.getType().struct);
			this.numOfFields = this.numOfFields + 1 + vars.size();
			addFldList(varCl.getType().struct);
		} else {
			Tab.insert(Obj.Var, varCl.getVarName(), varCl.getType().struct);
			report_info("Deklarisana promenljiva " + varCl.getVarName(), varCl);
			addVarsList(varCl.getType().struct);
		}
	}

	public void visit(VarDeclArrayClass varCl) {
		varDeclCount++;
		if (currentClass != null && currentMethod == null) {
			Struct formParamArrayType = new Struct(Struct.Array, varCl.getType().struct);
			Tab.insert(Obj.Fld, varCl.getVarName(), formParamArrayType);
			this.numOfFields = this.numOfFields + 1 + vars.size();
			addFldList(varCl.getType().struct);
		} else {
			Struct formParamArrayType = new Struct(Struct.Array, varCl.getType().struct);
			Tab.insert(Obj.Var, varCl.getVarName(), formParamArrayType);
			report_info("Deklarisana promenljiva " + varCl.getVarName(), varCl);
			addVarsList(varCl.getType().struct);
		}
	}

	public void visit(VarDeclListArrayClass arrayListCl) {
		varDeclCount++;
		vars.add(new ListVariables(true, arrayListCl.getVarName()));
		report_info("Deklarisana promenljiva " + arrayListCl.getVarName(), arrayListCl);
	}

	public void visit(VarDeclListClass arrayListCl) {
		varDeclCount++;
		vars.add(new ListVariables(false, arrayListCl.getVarName()));
		report_info("Deklarisana promenljiva " + arrayListCl.getVarName(), arrayListCl);
	}

	public void visit(ConstDeclClassNum constdecl) {
		varDeclCount++;
		if (constdecl.getType().struct != Tab.intType) {
			report_error("Greska na liniji " + constdecl.getLine() + ": dodeljena int vrednost pogresnom tipu", null);
			return;
		}
		report_info("Deklarisana konstanta " + constdecl.getName(), constdecl);
		Obj obj = Tab.insert(Obj.Con, constdecl.getName(), Tab.intType);
		obj.setAdr(constdecl.getVal());

		for (int i = 0; i < constData.size(); i++) {
			obj = Tab.insert(Obj.Con, constData.get(i).name, Tab.intType);
			obj.setAdr(constData.get(i).intVal);
		}
		constData.clear();
	}

	public void visit(ConstDeclClassChar constdecl) {
		varDeclCount++;
		if (constdecl.getType().struct != Tab.charType) {
			report_error("Greska na liniji " + constdecl.getLine() + ": dodeljena char vrednost pogresnom tipu", null);
			return;
		}
		report_info("Deklarisana konstanta " + constdecl.getName(), constdecl);
		Obj obj = Tab.insert(Obj.Con, constdecl.getName(), Tab.charType);
		obj.setAdr(constdecl.getVal());

		for (int i = 0; i < constData.size(); i++) {
			obj = Tab.insert(Obj.Con, constData.get(i).name, Tab.charType);
			obj.setAdr(constData.get(i).chVal);
		}
		constData.clear();
	}

	public void visit(ConstDeclClassBool constdecl) {
		varDeclCount++;
		boolean b = constdecl.getVal().equals("false") ? false : true;

		if (constdecl.getType().struct != boolType) {
			report_error("Greska na liniji " + constdecl.getLine() + ": dodeljena bool vrednost pogresnom tipu", null);
			return;
		}

		report_info("Deklarisana konstanta " + constdecl.getName(), constdecl);
		Obj obj = Tab.insert(Obj.Con, constdecl.getName(), boolType);
		if (!b)
			obj.setAdr(0);
		else
			obj.setAdr(1);

		for (int i = 0; i < constData.size(); i++) {
			obj = Tab.insert(Obj.Con, constData.get(i).name, boolType);
			obj.setAdr(constData.get(i).chVal);
		}
		constData.clear();
	}

	public void visit(ConstListClassNum constdecl) {
		varDeclCount++;
		constData.add(new ConstData(constdecl.getName(), constdecl.getVal()));
	}

	public void visit(ConstListClassChar constdecl) {
		varDeclCount++;
		constData.add(new ConstData(constdecl.getName(), constdecl.getVal()));
	}

	public void visit(ConstListClassBool constdecl) {
		varDeclCount++;
		boolean b = constdecl.getVal().equals("false") ? false : true;
		constData.add(new ConstData(constdecl.getName(), b));
	}

	public void visit(ArrayDeclClass array) {
		isArray = true;
	}

//=================================PROGRAM==========================================
	public void visit(ProgName ProgName) {
		ProgName.obj = Tab.insert(Obj.Prog, ProgName.getProgName(), Tab.noType);
		Tab.openScope();
	}

	public void visit(ProgramClass ProgramClass) {
		nVars = Tab.currentScope.getnVars();

		Tab.chainLocalSymbols(ProgramClass.getProgName().obj);
		Tab.closeScope();

		if (!mainMethod) {
			report_error("U programu mora postojati main metoda", ProgramClass);
		}

		if(!continueExists.isEmpty()) {
			report_error("Continue mora stajati u okviru do-while bloka", ProgramClass);
		}

		if (breakDetected) {
			report_error("Break mora stajati u okviru do-while ili switch bloka", ProgramClass);
		}
	}

//==============================TYPE=============================================
	String typeName ="";
	
	public void visit(TypeClass type) {
		Obj typeNode = Tab.find(type.getTypeName());
		typeName = type.getTypeName();
		
		if (typeNode == Tab.noObj) {
			report_error("Nije pronadjen tip " + type.getTypeName() + " u tabeli simbola! ", null);
			type.struct = Tab.noType;
		} else {
			if (Obj.Type == typeNode.getKind()) {
				type.struct = typeNode.getType();
			} else {
				report_error("Greska: Ime " + type.getTypeName() + " ne predstavlja tip!", type);
				type.struct = Tab.noType;
			}
		}
	}

	// ===============================PARAMETRI METODE==========================================

	public void visit(FormParsListClass formParList) {
		formPars++;
		if(formParList.getType().struct.getElemType() == null)
			formParsList.add(new ListVariables(formParList.getVarName(), formParList.getType().struct,Tab.find(typeName)));
		else
			formParsList.add(new ListVariables(formParList.getVarName(), new Struct(Struct.Array, formParList.getType().struct),Tab.find(typeName)));
	}

	public void visit(FormParsClass formPar) {
		formPars++; 
		if(formPar.getType().struct.getElemType() == null)
			formParsList.add(new ListVariables(formPar.getVarName(), formPar.getType().struct,Tab.find(typeName)));
		else
			formParsList.add(new ListVariables(formPar.getVarName(), new Struct(Struct.Array, formPar.getType().struct),Tab.find(typeName)));
		
		for (int i = formParsList.size() - 1; i >= 0; i--)
			Tab.insert(Obj.Var, formParsList.get(i).name, formParsList.get(i).s);

		formParsList.clear();

	}

	// ================================METODE===========================================

	boolean overrided = false;

	public void visit(MethodReturnNameClass MethodReturnNameClass) {

		report_info("Obradjuje se funkcija " + MethodReturnNameClass.getMName(), MethodReturnNameClass);
		returnFound = false;

		currentMethod = Tab.insert(Obj.Meth, MethodReturnNameClass.getMName(), MethodReturnNameClass.getType().struct);
		MethodReturnNameClass.obj = currentMethod;
		Tab.openScope();
		if (MethodReturnNameClass.getMName().equals("main"))
			report_error("*** Semanticka greska, main mora biti deklarisana kao void metoda bez argumenata",
					MethodReturnNameClass);
		if (currentClass != null)
			Tab.insert(Obj.Var, "this", currentClass.getType());
	}

	public void visit(MethodVoidClass MethodVoidClass) {
		report_info("Obradjuje se funkcija " + MethodVoidClass.getMName(), MethodVoidClass);
		returnFound = true;

		currentMethod = Tab.insert(Obj.Meth, MethodVoidClass.getMName(), Tab.noType);
		MethodVoidClass.obj = currentMethod;
		Tab.openScope();
		if (MethodVoidClass.getMName().equals("main")) {
			if (!mainMethod) {
				mainMethod = true;
			} else {
				report_error("*** Redefinisanje main metode nije dozvoljeno", MethodVoidClass);
			}
		}
		if (currentClass != null)
			Tab.insert(Obj.Var, "this", currentClass.getType());

	}

	// bez argumenata
	public void visit(MethodDeclClass MethodDeclClass) {
		if (!returnFound && currentMethod.getType() != Tab.noType) {
			report_error("Semanticka greska na liniji " + MethodDeclClass.getLine() + ": funkcija "
					+ currentMethod.getName() + " nema return iskaz!", null);
		}

		Tab.find(currentMethod.getName()).setLevel(formPars);
		currentMethod.setLevel(formPars);
		if(currentClass != null) currentMethod.setLevel(formPars + 1); 
		formPars = 0;
		Tab.chainLocalSymbols(currentMethod);
		Tab.closeScope();

		returnFound = false;
		currentMethod = null;

	}

	// sa argumentima
	public void visit(MethodDeclFParsClass MethodDeclFParsClass) {

		Tab.find(currentMethod.getName()).setLevel(formPars);
		currentMethod.setLevel(formPars);
		if(currentClass != null) currentMethod.setLevel(formPars + 1);
		formPars = 0;
		Tab.chainLocalSymbols(currentMethod);
		Tab.closeScope();
		currentMethod = null;

	}

	// for return statements
	public void visit(StatementRETExprClass returnExpr) {
		returnFound = true;
		Struct currMethType = currentMethod.getType();

		if (!currMethType.compatibleWith(returnExpr.getExpression().struct)) {
			report_error("Greska na liniji " + returnExpr.getLine() + " : "
					+ "tip izraza u return naredbi ne slaze se sa tipom povratne vrednosti funkcije "
					+ currentMethod.getName(), null);
		}
	}

	// ===========================================================================

	// sa argumentima
	public void visit(FuncParamCall funcParamCall) {
		Obj func = funcParamCall.getDesignator().obj;
		if (Obj.Meth == func.getKind()) {
			report_info("Pronadjen poziv funkcije sa argumentima " + func.getName() + " na liniji "
					+ funcParamCall.getLine(), null);
			funcParamCall.struct = func.getType();
		} else {
			if(checkIfMeth(calledFunc,Tab.find(currObj).getType()).getKind() != Obj.Meth) {
				report_error("Greska na liniji " + funcParamCall.getLine() + " : ime " + func.getName() + " nije funkcija!",
						null);
				funcParamCall.struct = Tab.noType;
			} else funcParamCall.struct = checkIfMeth(calledFunc,Tab.find(currObj).getType()).getType();
		}

	}

	// bez argumenata
	public void visit(FuncCall funcCall) {
		Obj func = funcCall.getDesignator().obj;
		if (Obj.Meth == func.getKind()) {
			report_info(
					"Pronadjen poziv funkcije bez argumenata " + func.getName() + " na liniji " + funcCall.getLine(),
					null);
			funcCall.struct = func.getType();
		} else {
			if(checkIfMeth(calledFunc,Tab.find(currObj).getType()).getKind() != Obj.Meth) {
				report_error("Greska na liniji " + funcCall.getLine() + " : ime " + func.getName() + " nije funkcija!",
						null);
				funcCall.struct = Tab.noType;
			} else funcCall.struct = checkIfMeth(calledFunc,Tab.find(currObj).getType()).getType();
		}

	}

	// ============================za proveru pozivanja fja=======================

	public void visit(DesignatorStatementPARENTClass funcCall) {
		Obj func = funcCall.getDesignator().obj;
		if(func == null) {
			report_error("Greska na liniji " + funcCall.getLine() + " nije moguce primeniti operator () nad tipom koji nije funkcija", null);
			return;
		}
		if (Obj.Meth == func.getKind() ) {
			if(func.getType() ==Tab.noType) {
				report_error("Greska na liniji " + funcCall.getLine() + " U izrazima se ne moze koristiti fja bez povratne vrednosti", null);
			}
			report_info("Pronadjen poziv funkcije bez argumenata " + func.getName() + " na liniji " + funcCall.getLine(),null);
		} else {
			if(checkIfMeth(calledFunc,Tab.find(currObj).getType()).getKind() != Obj.Meth)
				report_error("Greska na liniji " + funcCall.getLine() + " nije moguce primeniti operator () nad tipom koji nije funkcija", null);
		}
	}
	

	/*name predstavlja ime funkcije 
	 * a struct s predstavlja tip klase
	 */
	public Obj checkIfMeth(String name, Struct s) {
		if(s == null)
			return new Obj(0,"",null);
		if(s.getMembersTable().searchKey(name) != null)
			return s.getMembersTable().searchKey(name);
		
		else return checkIfMeth(name,s.getElemType());
		
	}
	
	
	/*name predstavlja ime funkcije 
	 * a struct s predstavlja tip klase
	 */
	public Struct getRetType(String name, Struct s) {
		if (s == null)
			return null;
		if(s.getMembersTable().searchKey(name) != null)
			return s.getMembersTable().searchKey(name).getType();
		else getRetType(name,s.getElemType());
		return null;
	}
	
	public boolean ifFuncExists(String name, Struct s) {
		if (s.getElemType() == null)
			return false;
		if (s.getElemType().getMembersTable().searchKey(name) != null)
			return false;
		else
			return ifFuncExists(name, s.getElemType());
	}

	public void visit(DesignatorStatementActParsClass funcCall) {
		Obj func = funcCall.getDesignator().obj;
		if (Obj.Meth == func.getKind()) { 
			report_info("Pronadjen poziv funkcije bez argumenata " + func.getName() + " na liniji " + funcCall.getLine(),null);
		} else {
			if(checkIfMeth(calledFunc,Tab.find(currObj).getType()).getKind() != Obj.Meth)
				report_error("Greska na liniji " + funcCall.getLine() + " nije moguce primeniti operator () nad tipom koji nije funkcija", null);
		}
	}

	// ===============================RAD SA DESIGNATORIMA=======================================

	boolean dotDetected = false;
	String calledFunc = "";
	String currObj = "";
	String detectedObj = "";

	public void visit(DesignatorListClass desCl) {
		dotDetected = true;//potrebno iz this clase ili niza izvaditi memberse i tu ga naci
		
		Obj obj = desCl.getDesignator().obj;
		if(obj == Tab.noObj || obj.getType().getKind()!= Struct.Class) {
			report_error(
					"Greska na liniji " + desCl.getLine() + " : Tip sa leve strane nije definisan",	null);
		}
		
		if(obj.getName() == "this" && currentClass != null) {
			Scope s = Tab.currentScope().getOuter();
			desCl.obj = s.getLocals().searchKey(desCl.getName());
		} else {
			if(obj.getType().getKind() == Struct.Array) {
				desCl.obj = obj.getType().getElemType().getMembersTable().searchKey(desCl.getName());
			} else {
				desCl.obj = obj.getType().getMembersTable().searchKey(desCl.getName());
			}
		}
		
		if(desCl.obj == null || desCl.obj == Tab.noObj) {
			report_error(
					"Greska na liniji " + desCl.getLine() + " : Ime " +desCl.getName()+ " ne postoji", null);
		}
		
		//desCl.obj = Tab.find(desCl.getName());
		calledFunc = desCl.getName();
		detectedObj = desCl.getName(); //u slucaju da je dozvoljeno ugnezdavanje niz[i].niz[j].j
	}

	public void visit(DotClass dc) {
		if (retStruct(Tab.find(detectedObj).getType()).getKind() != Struct.Class) {
		
			report_error(
					"Greska na liniji " + dc.getLine() + " : Tip sa leve strane mora biti klasnog tipa",
					null);
		}  
		currObj = detectedObj;
	}
	
	public Obj ifObjExists(String name, Struct s) { // za Obj.Meth i Obj.Fld za izvedenu kl
		if (s.getElemType() == null)
			return null;
		if (s.getElemType().getMembersTable().searchKey(name) != null)
			return s.getElemType().getMembersTable().searchKey(name);
		else
			return ifObjExists(name, s.getElemType());
	}

	public void visit(DesignatorClass designator) {
		Obj obj = Tab.find(designator.getName());
		detectedObj = designator.getName();
		
		if (obj == Tab.noObj) {
			if (currentClass != null) {
				obj = ifObjExists(designator.getName(), currentClass.getType());
			}
			if (obj == null)
				report_error("Greska na liniji " + designator.getLine() + " : ime " + designator.getName()
						+ " nije deklarisano! ", null);
		}
		designator.obj = obj;

		
	}

	public void visit(DesignatorListExprClass exprCl) {
		Struct temp = exprCl.getExpression().struct;
		if (temp.getElemType() == null) {
			if (temp != Tab.intType) {
				report_error("Greska na liniji " + exprCl.getLine() + " : Indeksiranje se mora vrsiti samo uz int tip",
						null);
			}
		} else {
			if (!isSameType(temp.getElemType(), Tab.intType)) {
				report_error("Greska na liniji " + exprCl.getLine() + " : Indeksiranje se mora vrsiti samo uz int tip",
						null);
			}
		} 
		
		exprCl.obj = new Obj(Obj.Elem, des.getName(), des.getType().getElemType());
		detectedObj = des.getName();
	}

	Obj des = null;
	
	public void visit(ArrName arrName) {
		des = arrName.getDesignator().obj;
	}
	
	public void visit(FactorDesignatorClass des) {
		des.struct = des.getDesignator().obj.getType();
	}
	

	public void visit(DesignatorStatementINCClass DesignatorStatementINCClass) {
		Obj obj = DesignatorStatementINCClass.getDesignator().obj;
		if (obj.getType().getKind() != 1) {
			report_error("Designator mora biti tipa int", DesignatorStatementINCClass);
		}
	}

	public void visit(DesignatorStatementDECClass DesignatorStatementDECClass) {
		Obj obj = DesignatorStatementDECClass.getDesignator().obj;
		if (obj.getType().getKind() != 1) {
			report_error("Designator mora biti tipa int", DesignatorStatementDECClass);
		}
	}

	// ===========================================================================

	public void visit(ExpressionExpr1Class expr) {
		expr.struct = expr.getExpression1().struct;
	}

	public void visit(Expression1Class expr) {
		expr.struct = expr.getTermList1().struct;
	}

	public void visit(NegTermClass negCl) {
		if(negCl.getTerm().struct != Tab.intType) {
			report_error("Greska na liniji " + negCl.getLine() + " Operand treba biti tipa int", null);
				}
		negCl.struct = Tab.intType;
	}
	
	public void visit(ExpressionClass exprCl) {
		if (exprCl.getExpression1().struct != boolType) {
			report_error("Greska na liniji " + exprCl.getLine() + " Operand sa leve strane ? treba da bude bool tipa",
					null);
		}
		if (exprCl.getExpression11().struct != exprCl.getExpression12().struct) {
			report_error("Greska na liniji " + exprCl.getLine()
					+ " Operandi sa leve i desne strane : treba da budu istog tipa", null);
		}
		exprCl.struct = exprCl.getExpression11().struct;
	}

	// =====================conditions===============================
	public void visit(CondFactRelopExprClass condCl) {
		if (!retStruct(condCl.getExpression().struct).compatibleWith(retStruct(condCl.getExpression1().struct))) {
			 Struct temp = getRetType(calledFunc, Tab.find(currObj).getType());
			if (temp != Tab.charType && temp != Tab.intType && temp != boolType)
			report_error("Greska na liniji " + condCl.getLine() + " : " + "Nekompatiblini tipovi ", null);
		}
		
		
		condCl.struct = boolType;
	}

	// =====================za dodelu======================================================

	public void visit(DesignatorStatementAssignopClass assignCl) {
		Obj obj = assignCl.getDesignator().obj; // provera da l je int, array or Fld
		if (obj != null && obj.getType().getElemType() == null) {
			if (!obj.getType().assignableTo(assignCl.getExpression().struct)) {
				if (!retStruct(obj.getType()).assignableTo(retStruct(assignCl.getExpression().struct)))
				report_error("Greska na liniji " + assignCl.getLine() + " : "
						+ "nekompatibilni izrazi sa leve i desne strane", null);
			}
		} else {
			if (!retStruct(obj.getType()).assignableTo(retStruct(assignCl.getExpression().struct))) { // !isSameType(obj.getType().getElemType(),assignCl.getExpression().struct)
				report_error("Greska na liniji " + assignCl.getLine() + " : "
						+ "nekompatibilni izrazi sa leve i desne strane", null);
			}
		}
	}

	
	// ================================continue&break&loop provera=============================

		boolean continueDetected = false; // za continue
	boolean breakDetected = false; // za break
	Stack<Boolean> continueExists = new Stack<>();

	public void visit(StatementDOWHILEClass loopClass) {
		if (loopClass.getCondition().struct != boolType) {
			report_error("Greska na liniji " + loopClass.getLine() + " Uslov treba biti tipa bool", null);
		}
		
		if(!this.continueExists.isEmpty()) this.continueExists.pop();
		if (breakDetected)
			breakDetected = false;
	}

	public void visit(StatementCONTINUEClass cont) {
		this.continueExists.push(true);
	}

	public void visit(StatementIFClass ifCl) {
		if (ifCl.getCondition().struct != boolType) {
			report_error("Greska na liniji " + ifCl.getLine() + " Uslov treba biti tipa bool", null);
		}
	}

	public void visit(StatementIFELSEClass ifCl) {
		if (ifCl.getCondition().struct != boolType) {
			report_error("Greska na liniji " + ifCl.getLine() + " Uslov treba biti tipa bool", null);
		}
	}

	Set<Integer> set = new HashSet<>();

	public void visit(StatementSWITCHCASEClass caseCl) {
		if (caseCl.getExpression().struct != Tab.intType) {
			report_error("Greska na liniji " + caseCl.getLine() + " Broj ponavljanja treba biti tipa int", null);
		}
		set.clear();
	}

	public void visit(CaseListClass caseCl) {

		if (set.contains(caseCl.getN())) {
			report_error("Greska na liniji " + caseCl.getLine()
					+ " Ne sme postojati vise case grana sa istom celobrojnom konstantom", null);
		} else
			set.add(caseCl.getN());

		if (breakDetected)
			breakDetected = false;
	}

	public void visit(StatementBREAKClass breakCl) {
		breakDetected = true;
	}

	// ==============================read klasa=============================================

	public void visit(StatementREADClass readCl) {
		Obj obj = readCl.getDesignator().obj;

		if (obj.getKind() != Obj.Var && obj.getKind() != Obj.Fld && obj.getKind() != Obj.Elem) {
			report_error("Greska na liniji " + readCl.getLine() + " : nekompatibilan tip read naredbe", null);
		}

		if (retStruct(obj.getType()) != Tab.charType && retStruct(obj.getType()) != Tab.intType
				&& retStruct(obj.getType()) != boolType) {
			report_error("Greska na liniji " + readCl.getLine() + " : nekompatibilan tip read naredbe", null);
		}

	}
	// =====================za proveru slicnost tipa gde se elemType ugnezdava==========

	public boolean isSameType(Struct s1, Struct s2) {
		if (s1.getElemType() == null) {
			if (s1 == s2)
				return true;
			else
				return false;
		} else
			isSameType(s1.getElemType(), s2);

		return false;
	}

	public Struct retStruct(Struct s) {
		if (s.getElemType() == null)
			return s;
		else
			return retStruct(s.getElemType());
	}

	// ===========================dodela structova  samo========================================

	public void visit(TermListClass t) {
		if (retStruct(t.getFactor().struct) != Tab.intType && retStruct(t.getTerm().struct) != Tab.intType) {
			report_error("Greska na liniji " + t.getLine() + " : Za operaciju */% oba operanda treba da budu int tipa",
					null);
		}
		t.struct = Tab.intType;
	}

	public void visit(FactorNTypeExprClass exprCl) {
		if (exprCl.getExpression().struct != Tab.intType) {
			report_error("Greska na liniji " + exprCl.getLine() + " Operand treba biti tipa int", null);
		}
		exprCl.struct = exprCl.getType().struct;
	}

	public void visit(FactorNTypeClass fact) {
		if (fact.getType().struct.getKind() != Struct.Class) {
			report_error("Greska na liniji " + fact.getLine() + " Treba biti klasa operand", null);
		}
		fact.struct = fact.getType().struct;
	}

	public void visit(ConditionClass cond) {
		cond.struct = cond.getCondTerm().struct;
	}

	public void visit(ConditionOrClass cond) {
		cond.struct = cond.getCondition().struct; //da li je svejedno condition ili condterm?
	}
	
	public void visit(ContTermAndClass cond) {
		cond.struct = cond.getCondTerm().struct; //da li je svejedno condition ili condterm?
	}
	public void visit(CondFactClass cond) {
		cond.struct = cond.getExpression().struct;
	}

	public void visit(ContTermClass cond) {
		cond.struct = cond.getCondFact().struct;
	}

	public void visit(TermClass term) {
		term.struct = term.getFactor().struct;
	}

	public void visit(TermList1Class termOpList) {
		Struct te = termOpList.getTermList1().struct;
		Struct t = termOpList.getTerm().struct;
		if (te.equals(t) && te == Tab.intType) {
			termOpList.struct = te;
		} else {
			report_error("Greska na liniji " + termOpList.getLine() + " : nekompatibilni tipovi u izrazu za sabiranje.",
					null);
			termOpList.struct = Tab.noType;
		}

	}

	public void visit(TermList1Signle term) {
		term.struct = term.getTerm().struct;
	}

	public void visit(ConstValueNUMClass numVal) {
		numVal.struct = Tab.intType;
	}

	public void visit(ConstValueCHARClass charVal) {
		charVal.struct = Tab.charType;
	}

	public void visit(ConstValueBOOLClass boolVal) {
		boolVal.struct = boolType;
	}

	public void visit(FactorConstValueClass constVal) {
		constVal.struct = constVal.getConstValue().struct;
	}

	public void visit(FactorExprClass expr) {
		expr.struct = expr.getExpression().struct;
	}

	public boolean passed() {
		return !errorDetected;
	}

}
