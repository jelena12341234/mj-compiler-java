// generated with ast extension for cup
// version 0.8
// 9/1/2021 17:29:36


package rs.ac.bg.etf.pp1.ast;

public class ExpressionExpr1Class extends Expression {

    private Expression1 Expression1;

    public ExpressionExpr1Class (Expression1 Expression1) {
        this.Expression1=Expression1;
        if(Expression1!=null) Expression1.setParent(this);
    }

    public Expression1 getExpression1() {
        return Expression1;
    }

    public void setExpression1(Expression1 Expression1) {
        this.Expression1=Expression1;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(Expression1!=null) Expression1.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(Expression1!=null) Expression1.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(Expression1!=null) Expression1.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ExpressionExpr1Class(\n");

        if(Expression1!=null)
            buffer.append(Expression1.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ExpressionExpr1Class]");
        return buffer.toString();
    }
}
