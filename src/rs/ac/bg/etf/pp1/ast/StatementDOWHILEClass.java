// generated with ast extension for cup
// version 0.8
// 9/1/2021 17:29:36


package rs.ac.bg.etf.pp1.ast;

public class StatementDOWHILEClass extends Statement {

    private DoClass DoClass;
    private Statement Statement;
    private WhileClass WhileClass;
    private Condition Condition;

    public StatementDOWHILEClass (DoClass DoClass, Statement Statement, WhileClass WhileClass, Condition Condition) {
        this.DoClass=DoClass;
        if(DoClass!=null) DoClass.setParent(this);
        this.Statement=Statement;
        if(Statement!=null) Statement.setParent(this);
        this.WhileClass=WhileClass;
        if(WhileClass!=null) WhileClass.setParent(this);
        this.Condition=Condition;
        if(Condition!=null) Condition.setParent(this);
    }

    public DoClass getDoClass() {
        return DoClass;
    }

    public void setDoClass(DoClass DoClass) {
        this.DoClass=DoClass;
    }

    public Statement getStatement() {
        return Statement;
    }

    public void setStatement(Statement Statement) {
        this.Statement=Statement;
    }

    public WhileClass getWhileClass() {
        return WhileClass;
    }

    public void setWhileClass(WhileClass WhileClass) {
        this.WhileClass=WhileClass;
    }

    public Condition getCondition() {
        return Condition;
    }

    public void setCondition(Condition Condition) {
        this.Condition=Condition;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(DoClass!=null) DoClass.accept(visitor);
        if(Statement!=null) Statement.accept(visitor);
        if(WhileClass!=null) WhileClass.accept(visitor);
        if(Condition!=null) Condition.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(DoClass!=null) DoClass.traverseTopDown(visitor);
        if(Statement!=null) Statement.traverseTopDown(visitor);
        if(WhileClass!=null) WhileClass.traverseTopDown(visitor);
        if(Condition!=null) Condition.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(DoClass!=null) DoClass.traverseBottomUp(visitor);
        if(Statement!=null) Statement.traverseBottomUp(visitor);
        if(WhileClass!=null) WhileClass.traverseBottomUp(visitor);
        if(Condition!=null) Condition.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("StatementDOWHILEClass(\n");

        if(DoClass!=null)
            buffer.append(DoClass.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Statement!=null)
            buffer.append(Statement.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(WhileClass!=null)
            buffer.append(WhileClass.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Condition!=null)
            buffer.append(Condition.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [StatementDOWHILEClass]");
        return buffer.toString();
    }
}
