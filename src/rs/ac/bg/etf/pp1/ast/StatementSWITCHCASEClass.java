// generated with ast extension for cup
// version 0.8
// 9/1/2021 17:29:36


package rs.ac.bg.etf.pp1.ast;

public class StatementSWITCHCASEClass extends Statement {

    private SwitchClass SwitchClass;
    private Expression Expression;
    private CaseList CaseList;

    public StatementSWITCHCASEClass (SwitchClass SwitchClass, Expression Expression, CaseList CaseList) {
        this.SwitchClass=SwitchClass;
        if(SwitchClass!=null) SwitchClass.setParent(this);
        this.Expression=Expression;
        if(Expression!=null) Expression.setParent(this);
        this.CaseList=CaseList;
        if(CaseList!=null) CaseList.setParent(this);
    }

    public SwitchClass getSwitchClass() {
        return SwitchClass;
    }

    public void setSwitchClass(SwitchClass SwitchClass) {
        this.SwitchClass=SwitchClass;
    }

    public Expression getExpression() {
        return Expression;
    }

    public void setExpression(Expression Expression) {
        this.Expression=Expression;
    }

    public CaseList getCaseList() {
        return CaseList;
    }

    public void setCaseList(CaseList CaseList) {
        this.CaseList=CaseList;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(SwitchClass!=null) SwitchClass.accept(visitor);
        if(Expression!=null) Expression.accept(visitor);
        if(CaseList!=null) CaseList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(SwitchClass!=null) SwitchClass.traverseTopDown(visitor);
        if(Expression!=null) Expression.traverseTopDown(visitor);
        if(CaseList!=null) CaseList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(SwitchClass!=null) SwitchClass.traverseBottomUp(visitor);
        if(Expression!=null) Expression.traverseBottomUp(visitor);
        if(CaseList!=null) CaseList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("StatementSWITCHCASEClass(\n");

        if(SwitchClass!=null)
            buffer.append(SwitchClass.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Expression!=null)
            buffer.append(Expression.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(CaseList!=null)
            buffer.append(CaseList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [StatementSWITCHCASEClass]");
        return buffer.toString();
    }
}
