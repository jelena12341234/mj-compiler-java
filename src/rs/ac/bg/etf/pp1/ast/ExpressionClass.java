// generated with ast extension for cup
// version 0.8
// 9/1/2021 17:29:36


package rs.ac.bg.etf.pp1.ast;

public class ExpressionClass extends Expression {

    private Expression1 Expression1;
    private QMark QMark;
    private Expression1 Expression11;
    private ColonClass ColonClass;
    private Expression1 Expression12;

    public ExpressionClass (Expression1 Expression1, QMark QMark, Expression1 Expression11, ColonClass ColonClass, Expression1 Expression12) {
        this.Expression1=Expression1;
        if(Expression1!=null) Expression1.setParent(this);
        this.QMark=QMark;
        if(QMark!=null) QMark.setParent(this);
        this.Expression11=Expression11;
        if(Expression11!=null) Expression11.setParent(this);
        this.ColonClass=ColonClass;
        if(ColonClass!=null) ColonClass.setParent(this);
        this.Expression12=Expression12;
        if(Expression12!=null) Expression12.setParent(this);
    }

    public Expression1 getExpression1() {
        return Expression1;
    }

    public void setExpression1(Expression1 Expression1) {
        this.Expression1=Expression1;
    }

    public QMark getQMark() {
        return QMark;
    }

    public void setQMark(QMark QMark) {
        this.QMark=QMark;
    }

    public Expression1 getExpression11() {
        return Expression11;
    }

    public void setExpression11(Expression1 Expression11) {
        this.Expression11=Expression11;
    }

    public ColonClass getColonClass() {
        return ColonClass;
    }

    public void setColonClass(ColonClass ColonClass) {
        this.ColonClass=ColonClass;
    }

    public Expression1 getExpression12() {
        return Expression12;
    }

    public void setExpression12(Expression1 Expression12) {
        this.Expression12=Expression12;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(Expression1!=null) Expression1.accept(visitor);
        if(QMark!=null) QMark.accept(visitor);
        if(Expression11!=null) Expression11.accept(visitor);
        if(ColonClass!=null) ColonClass.accept(visitor);
        if(Expression12!=null) Expression12.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(Expression1!=null) Expression1.traverseTopDown(visitor);
        if(QMark!=null) QMark.traverseTopDown(visitor);
        if(Expression11!=null) Expression11.traverseTopDown(visitor);
        if(ColonClass!=null) ColonClass.traverseTopDown(visitor);
        if(Expression12!=null) Expression12.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(Expression1!=null) Expression1.traverseBottomUp(visitor);
        if(QMark!=null) QMark.traverseBottomUp(visitor);
        if(Expression11!=null) Expression11.traverseBottomUp(visitor);
        if(ColonClass!=null) ColonClass.traverseBottomUp(visitor);
        if(Expression12!=null) Expression12.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ExpressionClass(\n");

        if(Expression1!=null)
            buffer.append(Expression1.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(QMark!=null)
            buffer.append(QMark.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Expression11!=null)
            buffer.append(Expression11.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ColonClass!=null)
            buffer.append(ColonClass.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Expression12!=null)
            buffer.append(Expression12.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ExpressionClass]");
        return buffer.toString();
    }
}
