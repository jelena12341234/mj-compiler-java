package rs.ac.bg.etf.pp1;

import rs.ac.bg.etf.pp1.ast.*;

public class CounterVisitor extends VisitorAdaptor {

	protected int count;
	
	public int getCount(){
		return count;
	}
	
	public static class FormParamCounter extends CounterVisitor{
	
		public void visit(FormParsClass formParamDecl){
			count++;
		}

		public void visit(FormParsListClass formParamDecl){
			count++;
		}
	
	}
	
	public static class VarCounter extends CounterVisitor{
		
		public void visit(VarDeclClass varDecl){
			count++;
		}

		public void visit(VarDeclArrayClass varDecl){
			count++;
		}
		
		
		public void visit(VarDeclListArrayClass varDecl){
			count++;
		}

		public void visit(VarDeclListClass varDecl){
			count++;
		}
		
	}
}
