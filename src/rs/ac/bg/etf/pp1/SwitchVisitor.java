package rs.ac.bg.etf.pp1;

import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.ast.VisitorAdaptor;
import java.util.*;

public class SwitchVisitor extends VisitorAdaptor {

	Stack<Integer> stack = new Stack<>();
	
	public void visit(CaseListClass clCl) {
		stack.push(clCl.getN());
	}

	public Stack<Integer> getStack() {
		return stack;
	}
	
	
	
}
