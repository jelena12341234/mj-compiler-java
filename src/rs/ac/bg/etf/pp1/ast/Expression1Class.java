// generated with ast extension for cup
// version 0.8
// 9/1/2021 17:29:36


package rs.ac.bg.etf.pp1.ast;

public class Expression1Class extends Expression1 {

    private TermList1 TermList1;

    public Expression1Class (TermList1 TermList1) {
        this.TermList1=TermList1;
        if(TermList1!=null) TermList1.setParent(this);
    }

    public TermList1 getTermList1() {
        return TermList1;
    }

    public void setTermList1(TermList1 TermList1) {
        this.TermList1=TermList1;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(TermList1!=null) TermList1.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(TermList1!=null) TermList1.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(TermList1!=null) TermList1.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("Expression1Class(\n");

        if(TermList1!=null)
            buffer.append(TermList1.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [Expression1Class]");
        return buffer.toString();
    }
}
