// generated with ast extension for cup
// version 0.8
// 9/1/2021 17:29:36


package rs.ac.bg.etf.pp1.ast;

public abstract class VisitorAdaptor implements Visitor { 

    public void visit(ArrayName ArrayName) { }
    public void visit(MethodDecl MethodDecl) { }
    public void visit(MethodReturnName MethodReturnName) { }
    public void visit(ProgDeclList ProgDeclList) { }
    public void visit(StatementList StatementList) { }
    public void visit(Factor Factor) { }
    public void visit(CondTerm CondTerm) { }
    public void visit(VarList VarList) { }
    public void visit(ConstList ConstList) { }
    public void visit(ClassDef ClassDef) { }
    public void visit(Designator Designator) { }
    public void visit(Term Term) { }
    public void visit(FormParsList FormParsList) { }
    public void visit(Condition Condition) { }
    public void visit(ConstValue ConstValue) { }
    public void visit(MulOp MulOp) { }
    public void visit(CaseList CaseList) { }
    public void visit(ActParsList ActParsList) { }
    public void visit(RelOp RelOp) { }
    public void visit(AssignOp AssignOp) { }
    public void visit(ArrayDecl ArrayDecl) { }
    public void visit(Expression1 Expression1) { }
    public void visit(VarDeclList VarDeclList) { }
    public void visit(ActPars ActPars) { }
    public void visit(AddOp AddOp) { }
    public void visit(DesignatorStatement DesignatorStatement) { }
    public void visit(Statement Statement) { }
    public void visit(TermList1 TermList1) { }
    public void visit(Expression Expression) { }
    public void visit(VarDecl VarDecl) { }
    public void visit(Type Type) { }
    public void visit(ClassDecl ClassDecl) { }
    public void visit(ConstDecl ConstDecl) { }
    public void visit(CondFact CondFact) { }
    public void visit(MethodDeclList MethodDeclList) { }
    public void visit(Program Program) { }
    public void visit(FormPars FormPars) { }
    public void visit(NoVarList NoVarList) { visit(); }
    public void visit(VarListClass VarListClass) { visit(); }
    public void visit(ErrorVarParam ErrorVarParam) { visit(); }
    public void visit(NoVarDeclList NoVarDeclList) { visit(); }
    public void visit(VarDeclListArrayClass VarDeclListArrayClass) { visit(); }
    public void visit(VarDeclListClass VarDeclListClass) { visit(); }
    public void visit(VarDeclArrayClass VarDeclArrayClass) { visit(); }
    public void visit(VarDeclClass VarDeclClass) { visit(); }
    public void visit(TermListClass TermListClass) { visit(); }
    public void visit(TermClass TermClass) { visit(); }
    public void visit(TypeClass TypeClass) { visit(); }
    public void visit(NoStatementList NoStatementList) { visit(); }
    public void visit(StatementListClass StatementListClass) { visit(); }
    public void visit(WhileClass WhileClass) { visit(); }
    public void visit(DoClass DoClass) { visit(); }
    public void visit(ElseClass ElseClass) { visit(); }
    public void visit(IfClass IfClass) { visit(); }
    public void visit(SwitchClass SwitchClass) { visit(); }
    public void visit(StatementListWBRACEClass StatementListWBRACEClass) { visit(); }
    public void visit(StatementPRINTNUMClass StatementPRINTNUMClass) { visit(); }
    public void visit(StatementPRINTClass StatementPRINTClass) { visit(); }
    public void visit(StatementREADClass StatementREADClass) { visit(); }
    public void visit(StatementRETExprClass StatementRETExprClass) { visit(); }
    public void visit(StatementRETClass StatementRETClass) { visit(); }
    public void visit(StatementCONTINUEClass StatementCONTINUEClass) { visit(); }
    public void visit(StatementBREAKClass StatementBREAKClass) { visit(); }
    public void visit(StatementSWITCHCASEClass StatementSWITCHCASEClass) { visit(); }
    public void visit(StatementDOWHILEClass StatementDOWHILEClass) { visit(); }
    public void visit(StatementIFELSEClass StatementIFELSEClass) { visit(); }
    public void visit(StatementIFClass StatementIFClass) { visit(); }
    public void visit(StatementDerived1 StatementDerived1) { visit(); }
    public void visit(StatementDesStatementClass StatementDesStatementClass) { visit(); }
    public void visit(LtEqClass LtEqClass) { visit(); }
    public void visit(LtOpClass LtOpClass) { visit(); }
    public void visit(GtEqOpClass GtEqOpClass) { visit(); }
    public void visit(GtOpClass GtOpClass) { visit(); }
    public void visit(DifOpClass DifOpClass) { visit(); }
    public void visit(EqualsOpClass EqualsOpClass) { visit(); }
    public void visit(ModOpClass ModOpClass) { visit(); }
    public void visit(DivOpClass DivOpClass) { visit(); }
    public void visit(MulOpClass MulOpClass) { visit(); }
    public void visit(NoMethodDeclList NoMethodDeclList) { visit(); }
    public void visit(MethodDeclListClass MethodDeclListClass) { visit(); }
    public void visit(MethodVoidClass MethodVoidClass) { visit(); }
    public void visit(MethodReturnNameClass MethodReturnNameClass) { visit(); }
    public void visit(MethodDeclFParsClass MethodDeclFParsClass) { visit(); }
    public void visit(MethodDeclClass MethodDeclClass) { visit(); }
    public void visit(FactorExprClass FactorExprClass) { visit(); }
    public void visit(FactorNTypeExprClass FactorNTypeExprClass) { visit(); }
    public void visit(FactorNTypeClass FactorNTypeClass) { visit(); }
    public void visit(FactorConstValueClass FactorConstValueClass) { visit(); }
    public void visit(FuncParamCall FuncParamCall) { visit(); }
    public void visit(FuncCall FuncCall) { visit(); }
    public void visit(FactorDesignatorClass FactorDesignatorClass) { visit(); }
    public void visit(NoFormParsList NoFormParsList) { visit(); }
    public void visit(FormParsListClass FormParsListClass) { visit(); }
    public void visit(ErrorFormParamDecl ErrorFormParamDecl) { visit(); }
    public void visit(FormParsClass FormParsClass) { visit(); }
    public void visit(NegTermClass NegTermClass) { visit(); }
    public void visit(TermList1Signle TermList1Signle) { visit(); }
    public void visit(TermList1Class TermList1Class) { visit(); }
    public void visit(Expression1ErrorClass Expression1ErrorClass) { visit(); }
    public void visit(Expression1Class Expression1Class) { visit(); }
    public void visit(ColonClass ColonClass) { visit(); }
    public void visit(QMark QMark) { visit(); }
    public void visit(ExpressionExpr1Class ExpressionExpr1Class) { visit(); }
    public void visit(ExpressionClass ExpressionClass) { visit(); }
    public void visit(DesignatorStatementDECClass DesignatorStatementDECClass) { visit(); }
    public void visit(DesignatorStatementINCClass DesignatorStatementINCClass) { visit(); }
    public void visit(DesignatorStatementActParsClass DesignatorStatementActParsClass) { visit(); }
    public void visit(DesignatorStatementPARENTClass DesignatorStatementPARENTClass) { visit(); }
    public void visit(DesignatorStatementAssignopClass DesignatorStatementAssignopClass) { visit(); }
    public void visit(ArrName ArrName) { visit(); }
    public void visit(DotClass DotClass) { visit(); }
    public void visit(DesignatorListExprClass DesignatorListExprClass) { visit(); }
    public void visit(DesignatorListClass DesignatorListClass) { visit(); }
    public void visit(DesignatorClass DesignatorClass) { visit(); }
    public void visit(ErrorCondition ErrorCondition) { visit(); }
    public void visit(ConditionDerived1 ConditionDerived1) { visit(); }
    public void visit(ConditionOrClass ConditionOrClass) { visit(); }
    public void visit(ConditionClass ConditionClass) { visit(); }
    public void visit(OrClass OrClass) { visit(); }
    public void visit(ContTermAndClass ContTermAndClass) { visit(); }
    public void visit(ContTermClass ContTermClass) { visit(); }
    public void visit(AndClass AndClass) { visit(); }
    public void visit(CondFactRelopExprClass CondFactRelopExprClass) { visit(); }
    public void visit(CondFactClass CondFactClass) { visit(); }
    public void visit(CaseClass CaseClass) { visit(); }
    public void visit(NoCaseList NoCaseList) { visit(); }
    public void visit(CaseListClass CaseListClass) { visit(); }
    public void visit(ErrorExtDecl ErrorExtDecl) { visit(); }
    public void visit(ClassDefExtClass ClassDefExtClass) { visit(); }
    public void visit(ClassDefClass ClassDefClass) { visit(); }
    public void visit(ClassDeclMTDListClass ClassDeclMTDListClass) { visit(); }
    public void visit(ClassDeclClass ClassDeclClass) { visit(); }
    public void visit(ConstValueBOOLClass ConstValueBOOLClass) { visit(); }
    public void visit(ConstValueCHARClass ConstValueCHARClass) { visit(); }
    public void visit(ConstValueNUMClass ConstValueNUMClass) { visit(); }
    public void visit(NoConstList NoConstList) { visit(); }
    public void visit(ConstListClassBool ConstListClassBool) { visit(); }
    public void visit(ConstListClassChar ConstListClassChar) { visit(); }
    public void visit(ConstListClassNum ConstListClassNum) { visit(); }
    public void visit(ConstDeclClassChar ConstDeclClassChar) { visit(); }
    public void visit(ConstDeclClassNum ConstDeclClassNum) { visit(); }
    public void visit(ConstDeclClassBool ConstDeclClassBool) { visit(); }
    public void visit(SubOpClass SubOpClass) { visit(); }
    public void visit(AddOpClass AddOpClass) { visit(); }
    public void visit(AssignOpClass AssignOpClass) { visit(); }
    public void visit(NoArrayDecl NoArrayDecl) { visit(); }
    public void visit(ArrayDeclClass ArrayDeclClass) { visit(); }
    public void visit(NoActParsList NoActParsList) { visit(); }
    public void visit(ActParsListClass ActParsListClass) { visit(); }
    public void visit(ActParsClass ActParsClass) { visit(); }
    public void visit(NoProgDeclList NoProgDeclList) { visit(); }
    public void visit(ProgDeclClassListClass ProgDeclClassListClass) { visit(); }
    public void visit(ProgDeclVarListClass ProgDeclVarListClass) { visit(); }
    public void visit(ProgDeclConstListClass ProgDeclConstListClass) { visit(); }
    public void visit(ProgName ProgName) { visit(); }
    public void visit(ProgramClass ProgramClass) { visit(); }


    public void visit() { }
}
